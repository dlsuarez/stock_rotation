# -*- coding: utf-8 -*-
###############################################################################
#    License, author and contributors information in:                         #
#    __openerp__.py file at the root folder of this module.                   #
###############################################################################

from openerp import models, fields, api
from openerp.tools.translate import _
from openerp.exceptions import Warning
import logging

class sale_historic(models.Model):

    _name = 'sale.historic'
    _description = _('Log the sale of a product in a point in history')

    # --------------------------- ENTITY  FIELDS ------------------------------

    product_id = fields.Many2one(
        string = 'Product',
        comodel_name = 'product.template'
    )

    product_uom_qty_sale = fields.Float(
        string = 'Product Unit of Measure in sale',
        default = 0.000,
        digits = (16, 3),
        help = 'Number of products sold for each sale'
    )

    standard_price_sale = fields.Float(
        string = 'Standard price sale',
        default = 0.00,
        digits = (16, 2),
        help = 'Standard price of products in a sale'
    )

    standard_price_stock = fields.Float(
        string = 'Standard price stock',
        default = 0.00,
        digits = (16, 2),
        help = 'Standard price of products in current stock'
    )
