# -*- coding: utf-8 -*-
###############################################################################
#    License, author and contributors information in:                         #
#    __openerp__.py file at the root folder of this module.                   #
###############################################################################

from openerp import models, fields, api
from openerp.tools.translate import _
from openerp.exceptions import Warning
import logging

class stock_historic(models.Model):

    _name = 'stock.historic'
    _description = _('Log the stock of a product in a point in history')

    # --------------------------- ENTITY  FIELDS ------------------------------

    product_id = fields.Many2one(
        string = 'Product',
        comodel_name = 'product.template'
    )

    product_uom_qty_move = fields.Float(
        string = 'Product Unit of Measure by movement',
        default = 0.000,
        digits = (16, 3),
        help = 'Number of products for each movement'
    )

    product_uom_qty_stock = fields.Float(
        string = 'Product Unit of Measure in stock',
        default = 0.000,
        digits = (16, 3),
        help = 'Current number of products in stock'
    )

    from_inventory = fields.Boolean(
        string = 'From inventory',
        default = False,
        help = 'Determines whether it is a movement of inventory'
    )

    from_purchase = fields.Boolean(
        string = 'From purchase',
        default = False,
        help = 'Determines whether it is a movement of a purchase'
    )

    from_sale = fields.Boolean(
        string = 'From sale',
        default = False,
        help = 'Determines whether it is a movement of a sale'
    )
