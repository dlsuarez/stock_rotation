# -*- coding: utf-8 -*-
###############################################################################
#    License, author and contributors information in:                         #
#    __openerp__.py file at the root folder of this module.                   #
###############################################################################

from openerp import models, fields, api
from openerp.tools.translate import _
from openerp.exceptions import Warning
import logging

class stock_move(models.Model):

    _inherit = 'stock.move'

    _logger = logging.getLogger(__name__)

    # ----------------------- AUXILIARY FIELD METHODS -------------------------

    def _check_stock_historic(self, values):
        from_inventory = False
        from_purchase = False
        from_sale = False
        if self.inventory_id:
            from_inventory = True
        if self.purchase_line_id:
            from_purchase = True
        if self.partner_id:
            from_sale = True
            self.env['sale.historic'].create(dict(
                product_id = self.product_id.id,
                product_uom_qty_sale = self.product_uom_qty,
                standard_price_sale = self.product_uom_qty * \
                    self.product_id.standard_price,
                standard_price_stock = self.product_id.qty_available * \
                    self.product_id.standard_price 
            ))
        self.env['stock.historic'].create(dict(
            product_id = self.product_id.id,
            product_uom_qty_move = self.product_uom_qty,
            product_uom_qty_stock = self.product_id.qty_available,
            from_inventory = from_inventory,
            from_purchase = from_purchase,
            from_sale = from_sale
        ))

    # ------------------------ METHODS  OVERWRITTEN ---------------------------

    @api.one
    def write(self, values):
        if 'state' in values and values['state'] == 'done':
            self._check_stock_historic(values)
        result = super(stock_move, self).write(values)
        return result
