# -*- coding: utf-8 -*-
###############################################################################
#    License, author and contributors information in:                         #
#    __openerp__.py file at the root folder of this module.                   #
###############################################################################

from openerp import models, fields, api
from openerp.tools.translate import _
from openerp.exceptions import Warning
from datetime import date, datetime, timedelta
import logging

class stock_rotation(models.Model):

    _name = 'stock.rotation'
    _description = _('Show the stock rotation of products in a period')

    _logger = logging.getLogger(__name__)

    # --------------------------- ENTITY  FIELDS ------------------------------

    date_start = fields.Datetime(
        string = 'Date start',
        required = False,
        default = lambda self: self._default_date_start()
    )

    date_end = fields.Datetime(
        string = 'Date end',
        required = False,
        default = lambda self: datetime.today()
    )

    results = fields.Html(
        string = 'Results',
        required = False,
        readonly = True,
        default = lambda self: "",
        help = False,
        translate = False
    )

    # ----------------------- AUXILIARY FIELD METHODS -------------------------

    def _default_date_start(self):
        query_str = """
            SELECT h.write_date
            FROM stock_historic AS h
            ORDER BY h.write_date ASC
            LIMIT 1
        """
        self.env.cr.execute(query_str)
        _dates = set(_date[0] for _date in self.env.cr.fetchall())
        if len(_dates) > 0:
            return tuple(_dates)[0]
        else:
            return datetime.now()

    # -------------------------- AUXILIARY METHODS ----------------------------

    def _historic_sold_products(self):
        return self.env['sale.historic'].search([]).mapped('product_id')

    def _historic_stock_products(self):
        return self.env['stock.historic'].search([]).mapped('product_id')

    def _get_sold_by_currency_units(self, product):
        result = 0.000
        products_sold = self.env['sale.historic'].search([
            ('product_id', '=', product.id),
            ('create_date', '>=', self.date_start),
            ('create_date', '<=', self.date_end)
        ])
        for product_sold in products_sold:
            result = result + float(product_sold.standard_price_sale)
        return float("{0:.3f}".format(result))

    def _get_stock_average_by_currency_units(self, product):
        total = 0.000
        products_stock = self.env['sale.historic'].search([
            ('product_id', '=', product.id),
            ('create_date', '>=', self.date_start),
            ('create_date', '<=', self.date_end)
        ])
        for product_stock in products_stock:
            total = total + float(product_stock.standard_price_stock)
        if len(products_stock) == 0:
            return 0.000
        return float("{0:.3f}".format(total / len(products_stock)))

    def _get_index_rotation_by_currency_units(self, product):
        sold = self._get_sold_by_currency_units(product)
        stock_average = self._get_stock_average_by_currency_units(product)
        if stock_average == 0:
            return 0.000
        return float("{0:.3f}".format(sold / stock_average))

    def _get_sold_by_physical_units(self, product):
        result = 0.000
        products_sold = self.env['stock.historic'].search([
            ('product_id', '=', product.id),
            ('from_sale', '=', True),
            ('create_date', '>=', self.date_start),
            ('create_date', '<=', self.date_end)
        ])
        for product_sold in products_sold:
            result = result + float(product_sold.product_uom_qty_move)
        return float("{0:.3f}".format(result))

    def _get_stock_average_by_physical_units(self, product):
        total = 0.000
        products_stock = self.env['stock.historic'].search([
            ('product_id', '=', product.id),
            ('create_date', '>=', self.date_start),
            ('create_date', '<=', self.date_end)
        ])
        for product_stock in products_stock:
            total = total + float(product_stock.product_uom_qty_stock)
        if len(products_stock) == 0:
            return 0.000
        return float("{0:.3f}".format(total / len(products_stock)))

    def _get_index_rotation_by_physical_units(self, product):
        sold = self._get_sold_by_physical_units(product)
        stock_average = self._get_stock_average_by_physical_units(product)
        if stock_average == 0:
            return 0.000
        return float("{0:.3f}".format(sold / stock_average))

    # ----------------------------- NEW METHODS -------------------------------

    @api.multi
    def render_stock_rotation_by_currency_units(self):
        html = ""
        html = html + "<table class='table'>"
        html = html + "<thead>"
        html = html + "<tr>"
        html = html + "<th>" + _('Product') + "</th>"
        html = html + "<th>" + _('Index rotation') + "</th>"
        html = html + "</tr>"
        html = html + "</thead>"
        html = html + "<tbody>"
        for product in self._historic_sold_products():
            html = html + "<tr>"
            html = html + "<td>" + str(product.name) + "</td>"
            html = html + "<td>" + \
                str(self._get_index_rotation_by_currency_units(product)) + \
                "</td>"
            html = html + "</tr>"
        html = html + "</tbody>"
        html = html + "</table>"
        self.results = html

    @api.multi
    def render_stock_rotation_by_physical_units(self):
        html = ""
        html = html + "<table class='table'>"
        html = html + "<thead>"
        html = html + "<tr>"
        html = html + "<th>" + _('Product') + "</th>"
        html = html + "<th>" + _('Index rotation') + "</th>"
        html = html + "</tr>"
        html = html + "</thead>"
        html = html + "<tbody>"
        for product in self._historic_stock_products():
            html = html + "<tr>"
            html = html + "<td>" + str(product.name) + "</td>"
            html = html + "<td>" + \
                str(self._get_index_rotation_by_physical_units(product)) + \
                "</td>"
            html = html + "</tr>"
        html = html + "</tbody>"
        html = html + "</table>"
        self.results = html
